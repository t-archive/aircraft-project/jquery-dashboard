$(document).ready(() => {


    const routing = new Routing();
    const dataManager = new DataManager();

    const buildModal = new BuildModal();

    const dataCreator = new DataCreator(buildModal);

    let allAircraftData = null;
    let civilAircraftData = null;
    let militaryAircraftData = null;


    dataManager.allAircraftObservable.subscribe((data) => {
        allAircraftData = data;
        routing.update(allAircraftData, civilAircraftData, militaryAircraftData);
    });
    dataManager.civilAircraftObservable.subscribe((data) => {
        civilAircraftData = data;
        routing.update(allAircraftData, civilAircraftData, militaryAircraftData);
    });
    dataManager.militaryAircraftObservable.subscribe((data) => {
        militaryAircraftData = data;
        routing.update(allAircraftData, civilAircraftData, militaryAircraftData);
    });

    routing.update(allAircraftData, civilAircraftData, militaryAircraftData);
    $(window).on("hashchange", () => {
        routing.update(allAircraftData, civilAircraftData, militaryAircraftData);
    });
        $(document).on("click", "th", function () {
            let row = $(this).attr('id').split('th-')[1];
            let sort = dataManager.getRepository().sort(row);
            routing.updatePage(sort);

        });

        $("#search-input").val("");
        $("#search-input").on("keyup", function() {
            let value = $(this).val().toLowerCase();
            let sort = dataManager.getRepository().filter(value);
            dataManager.getRepository().sort();
            routing.updatePage(sort);
        });
    this.myBuildModal = buildModal;
    this.myDataManager = dataManager;
    Action.controlWithKey();
    myAnimation.searchInput();
});
function onAircraftRowClicked(id) {
    let action = new Action();
    action.onEditAircraftClicked(id, this.myDataManager);

}
function openCreateModal() {
    let action = new Action();
    action.openCreateModal();
}
function selectCreateAircraft(value) {
    this.myBuildModal.getTextInput(value);
}
function openSearchModal() {
    Action.search(true);
}
function closeSearchModal() {
    Action.search(false);
}
function chartRowAircraft(id){
    let action = new Action();
    action.chartsAircraftClicked(id, this.myDataManager);
}



