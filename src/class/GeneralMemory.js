class GeneralMemory {
    /**
     * Search Settings for Similar Aircraft (Ähnliche Flugzeuge)
     */
    static range() {
        return {
            "distance": 1000,
            "seatCount": 100,
            "maxSeatCount": 25,
            "length": 2.5,
            "wingSpan": 2.5,
            "height": 2.5,
            "consumption": 0.75,
        };
    }
    static inCalculationUse() {
        return {
            "distance": true,
            "seatCount": true,
            "maxSeatCount": true,
            "length": false,
            "wingSpan": false,
            "height": false,
            "consumption": false,
        };
    }

    /**
     * ########################################## END ##########################################
     */
    static distanceEnd() {
        return "km";
    }
    static seatCountEnd() {
        return "PAX";
    }
    static maxSeatCountEnd() {
        return "PAX";
    }
    static lengthEnd() {
        return "m";
    }
    static wingSpanEnd() {
        return "m";
    }
    static heightEnd() {
        return "m";
    }
    static consumptionEnd() {
        return "l/100 km";
    }
    static title() {
        return {
            "name": "Name",
            "distance": "Reichweite",
            "seatCount": "Typische Sitzanzahl",
            "maxSeatCount": "Maximale Sitzanzahl",
            "length": "Länge",
            "wingSpan": "Flügelspannweite",
            "height": "Höhe",
            "consumption": "Verbrauch",
            "gateLicense": "Gate Lizenz",
            "flare": "Flare",
            "airRefueling": "Luftbetankung",
        };
    }
    static steps() {
        return {
            "distance": 100,
            "seatCount": 1,
            "maxSeatCount": 1,
            "length": 0.1,
            "wingSpan": 0.1,
            "height": 0.1,
            "consumption": 0.01,
        };
    }
}
