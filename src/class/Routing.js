class Routing {
    nowHash = window.location.hash;
    constructor() {

    }
    openAllPage(data) {
        new BuildAllAircraftTable(data, this.currentView());
    }
    openCivilPage(data) {
        new BuildCivilAircraftTable(data, this.currentView());
    }
    openMilitaryPage(data) {
        new BuildMilitaryAircraftTable(data, this.currentView());
    }

    currentView() {
        return this.nowHash;
    }
    updateHash() {
        this.nowHash = window.location.hash;
    }
    map() {
        return {
            none: "",
            allAircraft: "#all",
            civilAircraft: "#civil",
            militaryAircraft: "#military"
        };
    }
    updateAllPage(data) {
        this.openAllPage(data);
        document.getElementById("id_all").classList.add("active");
        document.getElementById("id_civil").classList.remove("active");
        document.getElementById("id_military").classList.remove("active");
    }
    updateCivilPage(data) {

        this.openCivilPage(data);
        document.getElementById("id_all").classList.remove("active");
        document.getElementById("id_civil").classList.add("active");
        document.getElementById("id_military").classList.remove("active");
    }
    updateMilitaryPage(data) {
        this.openMilitaryPage(data);
        document.getElementById("id_all").classList.remove("active");
        document.getElementById("id_civil").classList.remove("active");
        document.getElementById("id_military").classList.add("active");
    }
    updatePage(data) {
        this.updateHash();
        if (this.nowHash === this.map().allAircraft) {
            this.updateAllPage(data);
        } else if (this.nowHash === this.map().civilAircraft) {
            this.updateCivilPage(data);
        } else if (this.nowHash === this.map().militaryAircraft) {
            this.updateMilitaryPage(data);
        } else {
            this.updateAllPage(data);
        }

    }
    setPage(location) {
        if(Object.values(this.map()).includes(location)) {
            window.location = location;
            this.updateHash();
        }
    }
    checkWhatDataNeed(all,civil,military) {
        this.updateHash();
        if (this.nowHash === this.map().allAircraft) {
            return all;
        } else if (this.nowHash === this.map().civilAircraft) {
            return civil;
        } else if (this.nowHash === this.map().militaryAircraft) {
            return military;
        } else {
            this.setPage(this.map().allAircraft);
        }
    }
    update(allAircraftData, civilAircraftData, militaryAircraftData) {
        let DataNeed = this.checkWhatDataNeed(allAircraftData, civilAircraftData, militaryAircraftData);
        this.updatePage(DataNeed);
    }

}