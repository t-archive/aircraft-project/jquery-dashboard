class myAnimation {

    static searchInput() {

        let searchColor = "dark";
        $("#btnSearch").addClass("btn-" + searchColor);
        $("#search").addClass("bg-" + searchColor);
        $("#closeTab").addClass("btn-" + searchColor);

        $('#search-input').on({
            focus: function () {
                $(".searchText").addClass('focused');
            },
            blur: function () {
                $(".searchText").removeClass('focused');
            }
        });
    }


    static LoadingBar(stat) {
        this.position = 1;
        this.positionAction = true;
        let activeRowID = null;
        if (stat === false) {
            this.positionAction = false;

            // $(".loadAnimation").animate({left: "50%", right: "50%"}, 0);
            // $(".loadAnimationBlue").animate({left: "50%", right: "50%"}, 0);
            // $(".loadAnimationRed").animate({left: "50%", right: "50%"}, 0);


        }

        let $frames = 200;
        let $framesB = $frames;
        let $framesR = $frames;



        $(".loadAnimationBackground").removeClass("hide");
        setInterval(() => {
            if (this.position === 1) {
                $(".loadAnimation").animate({left: "22.5%", right: "72.5%"}, $frames);
                $(".loadAnimationBlue").animate({left: "47.5%", right: "47.5%"}, $framesB);
                $(".loadAnimationRed").animate({left: "72.5%", right: "22.5%"}, $framesR);

                $(".loadAnimation").animate({left: "0%", right: "75%"}, $frames);
                $(".loadAnimationBlue").animate({left: "25%", right: "25%"}, $framesB);
                $(".loadAnimationRed").animate({left: "75%", right: "0%"}, $framesR);
                this.position = 2;
            } else if (this.position === 2) {
                $(".loadAnimationRed").animate({left: "22.5%", right: "72.5%"}, $frames);
                $(".loadAnimation").animate({left: "47.5%", right: "47.5%"}, $framesB);
                $(".loadAnimationBlue").animate({left: "72.5%", right: "22.5%"}, $framesR);

                $(".loadAnimationRed").animate({left: "0%", right: "75%"}, $frames);
                $(".loadAnimation").animate({left: "25%", right: "25%"}, $framesB);
                $(".loadAnimationBlue").animate({left: "75%", right: "0%"}, $framesR);
                this.position = 3;

            } else if (this.position === 3) {
                $(".loadAnimationBlue").animate({left: "22.5%", right: "72.5%"}, $frames);
                $(".loadAnimationRed").animate({left: "47.5%", right: "47.5%"}, $framesB);
                $(".loadAnimation").animate({left: "72.5%", right: "22.5%"}, $framesR);

                $(".loadAnimationBlue").animate({left: "0%", right: "75%"}, $frames);
                $(".loadAnimationRed").animate({left: "25%", right: "25%"}, $framesB);
                $(".loadAnimation").animate({left: "75%", right: "0%"}, $framesR);
                this.position = 1;
            } else if (this.position === 0) {
                if(this.positionCopy === 2) {
                    // $(".loadAnimationBlue").animate({left: "0%", right: "100%"}, 500);
                    $(".loadAnimation").animate({left: "0%", right: "100%"}, $frames);
                    $(".loadAnimationBlue").animate({left: "0%", right: "0%"}, $framesB);
                    $(".loadAnimationRed").animate({left: "100%", right: "0%"}, $framesR);
                } else if (this.positionCopy === 3) {
                    // $(".loadAnimation").animate({left: "100%", right: "100%"}, 500);
                    $(".loadAnimation").animate({left: "0%", right: "0%"}, $frames);
                    $(".loadAnimationBlue").animate({left: "0%", right: "100%"}, $framesB);
                    $(".loadAnimationRed").animate({left: "100%", right: "0%"}, $framesR);
                } else if (this.positionCopy === 1) {
                    // $(".loadAnimationRed").animate({left: "0%", right: "100%"}, 500);
                    $(".loadAnimation").animate({left: "100%", right: "0%"}, $frames);
                    $(".loadAnimationBlue").animate({left: "0%", right: "100%"}, $framesB);
                    $(".loadAnimationRed").animate({left: "0%", right: "0%"}, $framesR);
                }
                this.positionCopy = this.position;
            }

            if (this.positionAction === false) {
                console.log(this.position);
                this.positionAction = true;


                this.positionCopy = this.position;

                this.position = 0;
                setTimeout(() => {
                    $(".loadAnimationBackground").addClass("hide");
                }, $frames * 6)

            }


        }, $frames * 6);

    }





}