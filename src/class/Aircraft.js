class Aircraft {


    distanceProperty = new PropertyComparison();
    seatCountProperty = new PropertyComparison();
    maxSeatCountProperty = new PropertyComparison();
    lengthProperty = new PropertyComparison();
    wingSpanProperty = new PropertyComparison();
    heightProperty = new PropertyComparison();
    consumptionProperty = new PropertyComparison();

    constructor(name, distance, seatCount, maxSeatCount, length, wingSpan, height, consumption) {
        this.name = name;
        this.distance = distance;
        this.seatCount = seatCount;
        this.maxSeatCount = maxSeatCount;
        this.length = length;
        this.wingSpan = wingSpan;
        this.height = height;
        this.consumption = consumption;
    }

    setID(id) {
        if(Number.isInteger(id) === true) {
            this.id = id;
        } else {
            this.id = null;
        }

    }

    containsSearchValue(value) {

        return this.name.toLowerCase().indexOf(value) > -1
        || this.distance.toString().toLowerCase().indexOf(value) > -1
        || this.seatCount.toString().toLowerCase().indexOf(value) > -1
        || this.maxSeatCount.toString().toLowerCase().indexOf(value) > -1
        || this.length.toString().toLowerCase().indexOf(value) > -1
        || this.wingSpan.toString().toLowerCase().indexOf(value) > -1
        || this.height.toString().toLowerCase().indexOf(value) > -1
        || this.consumption.toString().toLowerCase().indexOf(value) > -1;
    }
}

