class Subject {
    constructor() {
        this.subscribers = [];
    }

    subscribe(subscriberFunction) {
        this.subscribers.push(subscriberFunction);
        subscriberFunction(this.data);

    }

    notify(data) {
        this.data = data;
        this._emitToAll();

    }
    _emitToAll() {
        for(const subscriberFunction of this.subscribers) {
            subscriberFunction(this.data);
        }
    }
}