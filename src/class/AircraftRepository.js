class AircraftRepository {

    constructor() {
        this.version = null;
        this.aircrafts = [];
        this.sortetAircrafts = [];
        if (this.stat === undefined || this.stat === null) {
            this.stat = -1;
        }

        this.key = undefined;
        this.averageData = new AverageData();
    }

    add(aircraft) {
        this.aircrafts.push(aircraft);
    }

    start() {
        this.sortetAircrafts = this.aircrafts.slice();
        this.sort();
        this.generate();
    }

    clear() {
        this.aircrafts.length = 0;
    }


    generate() {
        this.averageData.start(this.aircrafts);
        this.minData = this.averageData.minData;
        this.maxData = this.averageData.maxData;
    }

    compileToPercent(aircraft) {

        let distance = (100 * (aircraft.distance - this.minData.distance)) / (this.maxData.distance - this.minData.distance);
        let seatCount = (100 * (aircraft.seatCount - this.minData.seatCount)) / (this.maxData.seatCount - this.minData.seatCount);
        let maxSeatCount = (100 * (aircraft.maxSeatCount - this.minData.maxSeatCount)) / (this.maxData.maxSeatCount - this.minData.maxSeatCount);
        let length = (100 * (aircraft.length - this.minData.length)) / (this.maxData.length - this.minData.length);
        let wingSpan = (100 * (aircraft.wingSpan - this.minData.wingSpan)) / (this.maxData.wingSpan - this.minData.wingSpan);
        let height = (100 * (aircraft.height - this.minData.height)) / (this.maxData.height - this.minData.height);
        let consumption = (100 * (aircraft.consumption - this.minData.consumption)) / (this.maxData.consumption - this.minData.consumption);

        let distanceReserve = 100 - distance;
        let seatCountReserve = 100 - seatCount;
        let maxSeatCountReserve = 100 - maxSeatCount;
        let lengthReserve = 100 - length;
        let wingSpanReserve = 100 - wingSpan;
        let heightReserve = 100 - height;
        let consumptionReserve = 100 - consumption;


        return {
            "aircraft": {
                "distance": distance,
                "seatCount": seatCount,
                "maxSeatCount": maxSeatCount,
                "length": length,
                "wingSpan": wingSpan,
                "height": height,
                "consumption": consumption
            },
            "aircraftReserve": {
                "distance": distanceReserve,
                "seatCount": seatCountReserve,
                "maxSeatCount": maxSeatCountReserve,
                "length": lengthReserve,
                "wingSpan": wingSpanReserve,
                "height": heightReserve,
                "consumption": consumptionReserve
            }
        };
    }


    sort(key = null) {
        if (key !== null) {
            this.stat = this.stat + 1;
        }
        return this.sortByKey(key);
    }


    filter(search) {
        let result = this.aircrafts.filter((aircraft) => {
            return aircraft.containsSearchValue(search.toLowerCase());
        });
        this.sortetAircrafts = result;
        return result;
    }
    getAircraftByID(id) {
        let result = null;
        for (const item of this.aircrafts) {
            if (item.id === id) {
                result = item;
                break;
            }
        }
        return result;
    }
    getSimilarAircraft(aircraft) {
        let result = [];
        for (const item of this.aircrafts) {
            if (item.id === aircraft.id) {
                continue;
            }
            if (
                ((GeneralMemory.inCalculationUse().distance) ? Action.inRange(item.distance, GeneralMemory.range().distance, aircraft.distance) : true) &&
                ((GeneralMemory.inCalculationUse().seatCount) ? Action.inRange(item.seatCount, GeneralMemory.range().seatCount, aircraft.seatCount) : true) &&
                ((GeneralMemory.inCalculationUse().maxSeatCount) ? Action.inRange(item.maxSeatCount, GeneralMemory.range().maxSeatCount, aircraft.maxSeatCount) : true) &&
                ((GeneralMemory.inCalculationUse().length) ? Action.inRange(item.length, GeneralMemory.range().length, aircraft.length) : true) &&
                ((GeneralMemory.inCalculationUse().wingSpan) ? Action.inRange(item.wingSpan, GeneralMemory.range().wingSpan, aircraft.wingSpan) : true) &&
                ((GeneralMemory.inCalculationUse().height) ? Action.inRange(item.height, GeneralMemory.range().height, aircraft.height) : true) &&
                ((GeneralMemory.inCalculationUse().consumption) ? Action.inRange(item.consumption, GeneralMemory.range().consumption, aircraft.consumption) : true)
            ) {
                result.push(item);
            }
        }
        return result;
    }
    sortByKey(key = null) {
        if (this.key !== undefined) {
            if (key !== this.key && key !== null) {
                this.stat = 0;
            }
        }
        let result;
        if (key === null) {
            key = this.key;
        } else {
            this.key = key;
        }


        if (this.stat === 0) {
            $(document).ready(() => {
                $("#th-" + key + " #icon").removeClass("fa-sort");
                $("#th-" + key + " #icon").removeClass("fa-sort-up");
                $("#th-" + key + " #icon").addClass("fa-sort-down");
            });
            if (this.sortetAircrafts.length === 0) {
                this.sortetAircrafts = this.aircrafts.slice();
            }

            result = this.sortAction(key, false);
        } else if (this.stat === 1) {
            $(document).ready(() => {
                $("#th-" + key + " #icon").removeClass("fa-sort");
                $("#th-" + key + " #icon").removeClass("fa-sort-down");
                $("#th-" + key + " #icon").addClass("fa-sort-up");
            });
            if (this.sortetAircrafts.length === 0) {
                this.sortetAircrafts = this.aircrafts.slice();
            }
            result = this.sortAction(key, true);
        } else if (this.stat === 2) {
            $("#th-" + key + " #icon").addClass("fa-sort");
            $("#th-" + key + " #icon").removeClass("fa-sort-down");
            $("#th-" + key + " #icon").removeClass("fa-sort-up");
            this.stat = -1;
            if (this.sortetAircrafts.length === 0) {
                this.sortetAircrafts = this.aircrafts.slice();
            }
            this.sortetAircrafts = this.aircrafts.slice();
            result = this.sortetAircrafts;
        }

        return result;
    }
    sortAction(key, asc) {

        return this.sortetAircrafts.sort((a, b) => {
            let x = a[key];
            let y = b[key];

            if (y === undefined) {
                y = -1;
                if (key === "gateLicense") {
                    return;
                }

            }
            if (x === undefined) {
                x = -1;

                if (key === "airRefueling") {
                    x = 2;

                }
                if (key === "flare") {
                    x = 2;

                }
            }
            let result = ((x > y) ? -1 : ((x < y) ? 1 : 0));
            if (asc === false) {
                result = result * -1;
            }

            return result;

        });
    }
}