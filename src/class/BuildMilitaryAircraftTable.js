class BuildMilitaryAircraftTable {
    constructor(data, currentView) {
        if (!data) {
            return;
        }

        let trHTML = "";
        trHTML += '<div class="card border-0 shadow my-5">\n' +
            '<div class="card-body p-5">\n' +
            '<h1 class="font-weight-light">Military Aircraft\'s </h1>\n' +
            '<p class="lead"></p>\n' +
            '<p class="lead"></p>\n' +
            '<div class="table-responsive">\n' +
            '<table class="table table-sm table-bordered" id="table">\n';
        trHTML += '<thead><tr>\n' +
            '<th id="th-name"><i id="icon" class="fas fa-sort"></i> Name</th>\n' +
            '<th id="th-distance"><i id="icon" class="fas fa-sort"></i> Reichweite</th>\n' +
            '<th id="th-seatCount"><i id="icon" class="fas fa-sort"></i> Typische Sitzanzahl</th>\n' +
            '<th id="th-maxSeatCount"><i id="icon" class="fas fa-sort"></i> Maximale Sitzanzahl</th>\n' +
            '<th id="th-length"><i id="icon" class="fas fa-sort"></i> Länge</th>\n' +
            '<th id="th-wingSpan"><i id="icon" class="fas fa-sort"></i> Flügelspannweite</th>\n' +
            '<th id="th-height"><i id="icon" class="fas fa-sort"></i> Höhe</th>\n' +
            '<th id="th-consumption"><i id="icon" class="fas fa-sort"></i> Verbrauch</th>\n' +
            '<th id="th-flare"><i id="icon" class="fas fa-sort"></i> Flare</th>\n' +
            '<th id="th-airRefueling"><i id="icon" class="fas fa-sort"></i> Luftbetankung</th>\n' +
            '<th class="buttonTH"></th>\n' +
            '</tr></thead>';

        for (const value of data) {
            if (value instanceof MilitaryAircraft) {
                trHTML += '<tr id="' + value.id + '" class="aircraft-row"  >' +
                    '<td><strong>' + value.name + '</strong></td>' +
                    '<td>' + value.distance.toLocaleString() + ' ' + GeneralMemory.distanceEnd() + '</td>' +
                    '<td>' + value.seatCount.toLocaleString() + ' ' + GeneralMemory.seatCountEnd() + '</td>' +
                    '<td>' + value.maxSeatCount.toLocaleString() + ' ' + GeneralMemory.maxSeatCountEnd() + '</td>' +
                    '<td>' + value.length.toLocaleString() + ' ' + GeneralMemory.lengthEnd() + '</td>' +
                    '<td>' + value.wingSpan.toLocaleString() + ' ' + GeneralMemory.wingSpanEnd() + '</td>' +
                    '<td>' + value.height.toLocaleString() + ' ' + GeneralMemory.heightEnd() + '</td>' +
                    '<td>' + value.consumption.toLocaleString() + ' ' + GeneralMemory.consumptionEnd() + '</td>' +

                    '<td>' + ((value.flare == 1) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-times text-danger'></i>") + '</td>' +
                    '<td>' + ((value.airRefueling == 1) ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-times text-danger'></i>") + '</td>' +
                    '<td><a href="'+ currentView +'"  onclick="onAircraftRowClicked('+value.id+')" class="btn btn-warning btn btn-badge btn-sm"><i class="fas fa-edit"></i></a>' +
                    ' <a href="'+ currentView +'"  onclick="chartRowAircraft('+value.id+')" class="btn btn-primary btn btn-badge btn-sm"><i class="fas fa-chart-pie"></i></a></td>' +

                    '' +
                    '</tr>';
            }


        }


        trHTML += '</table>\n' +
            '</div>\n' +
            '<p class="lead mb-0"></p>\n' +
            '</div>\n' +
            '</div>';
        $('#box').html(trHTML);
    }

}