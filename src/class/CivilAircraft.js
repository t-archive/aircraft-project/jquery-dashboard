class CivilAircraft extends Aircraft {
    setGateLicense(gateLicense) {
        this.gateLicense = gateLicense;
    }

    getCivilAircraft() {
        let array = {
            "id": this.id,
            "name": this.name,
            "distance": this.distance,
            "seatCount": this.seatCount,
            "maxSeatCount": this.maxSeatCount,
            "length": this.length,
            "wingSpan": this.wingSpan,
            "height": this.height,
            "consumption": this.consumption,
            "gateLicense": this.gateLicense
        };
        if (isNaN(parseInt(this.id))) {
            delete array["id"];
        }
        return array;
    }

    makeJSON() {
        let Array = {
            "aircraft": {
            }
        };
        Array.aircraft = this.getCivilAircraft();
        return Array;
    }


}