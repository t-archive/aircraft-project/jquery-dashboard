class AverageData {

    distance = 0;
    seatCount = 0;
    maxSeatCount = 0;
    length = 0;
    wingSpan = 0;
    height = 0;
    consumption = 0;

    distancePercent = 0;
    seatCountPercent = 0;
    maxSeatCountPercent = 0;
    lengthPercent = 0;
    wingSpanPercent = 0;
    heightPercent = 0;
    consumptionPercent = 0;

    distancePercentReserve = 0;
    seatCountPercentReserve = 0;
    maxSeatCountPercentReserve = 0;
    lengthPercentReserve = 0;
    wingSpanPercentReserve = 0;
    heightPercentReserve = 0;
    consumptionPercentReserve = 0;



    start(aircrafts) {
        this.aircrafts = aircrafts;
        let distance = 0;
        let seatCount = 0;
        let maxSeatCount = 0;
        let length = 0;
        let wingSpan = 0;
        let height = 0;
        let consumption = 0;
        let count = 0;
        let distanceMin = Number.POSITIVE_INFINITY;
        let seatCountMin = Number.POSITIVE_INFINITY;
        let maxSeatCountMin = Number.POSITIVE_INFINITY;
        let lengthMin = Number.POSITIVE_INFINITY;
        let wingSpanMin = Number.POSITIVE_INFINITY;
        let heightMin = Number.POSITIVE_INFINITY;
        let consumptionMin = Number.POSITIVE_INFINITY;
        let distanceMax = 0;
        let seatCountMax = 0;
        let maxSeatCountMax = 0;
        let lengthMax = 0;
        let wingSpanMax = 0;
        let heightMax = 0;
        let consumptionMax = 0;

        for (let aircraft of this.aircrafts) {
            count = count + 1;
            distance = distance + aircraft.distance;
            seatCount = seatCount + aircraft.seatCount;
            maxSeatCount = maxSeatCount + aircraft.maxSeatCount;
            length = length + aircraft.length;
            wingSpan = wingSpan + aircraft.wingSpan;
            height = height + aircraft.height;
            consumption = consumption + aircraft.consumption;

            distanceMin = Math.min(distanceMin, aircraft.distance);
            seatCountMin = Math.min(seatCountMin, aircraft.seatCount);
            maxSeatCountMin = Math.min(maxSeatCountMin, aircraft.maxSeatCount);
            lengthMin = Math.min(lengthMin, aircraft.length);
            wingSpanMin = Math.min(wingSpanMin, aircraft.wingSpan);
            heightMin = Math.min(heightMin, aircraft.height);
            consumptionMin = Math.min(consumptionMin, aircraft.consumption);

            distanceMax = Math.max(distanceMax, aircraft.distance);
            seatCountMax = Math.max(seatCountMax, aircraft.seatCount);
            maxSeatCountMax = Math.max(maxSeatCountMax, aircraft.maxSeatCount);
            lengthMax = Math.max(lengthMax, aircraft.length);
            wingSpanMax = Math.max(wingSpanMax, aircraft.wingSpan);
            heightMax = Math.max(heightMax, aircraft.height);
            consumptionMax = Math.max(consumptionMax, aircraft.consumption);

            for (const otherAircraft of this.aircrafts) {
                if (aircraft !== otherAircraft) {
                    if (otherAircraft.distance > aircraft.distance) {
                        aircraft.distanceProperty.counterBetter++;
                    } else if (otherAircraft.distance === aircraft.distance) {
                        aircraft.distanceProperty.counterEqual++;
                    } else if (otherAircraft.distance < aircraft.distance) {
                        aircraft.distanceProperty.counterWorse++;
                    }

                    if (otherAircraft.seatCount > aircraft.seatCount) {
                        aircraft.seatCountProperty.counterBetter++;
                    } else if (otherAircraft.seatCount === aircraft.seatCount) {
                        aircraft.seatCountProperty.counterEqual++;
                    } else if (otherAircraft.seatCount < aircraft.seatCount) {
                        aircraft.seatCountProperty.counterWorse++;
                    }

                    if (otherAircraft.maxSeatCount > aircraft.maxSeatCount) {
                        aircraft.maxSeatCountProperty.counterBetter++;
                    } else if (otherAircraft.maxSeatCount === aircraft.maxSeatCount) {
                        aircraft.maxSeatCountProperty.counterEqual++;
                    } else if (otherAircraft.maxSeatCount < aircraft.maxSeatCount) {
                        aircraft.maxSeatCountProperty.counterWorse++;
                    }

                    if (otherAircraft.length > aircraft.length) {
                        aircraft.lengthProperty.counterBetter++;
                    } else if (otherAircraft.length === aircraft.length) {
                        aircraft.lengthProperty.counterEqual++;
                    } else if (otherAircraft.length < aircraft.length) {
                        aircraft.lengthProperty.counterWorse++;
                    }

                    if (otherAircraft.wingSpan > aircraft.wingSpan) {
                        aircraft.wingSpanProperty.counterBetter++;
                    } else if (otherAircraft.wingSpan === aircraft.wingSpan) {
                        aircraft.wingSpanProperty.counterEqual++;
                    } else if (otherAircraft.wingSpan < aircraft.wingSpan) {
                        aircraft.wingSpanProperty.counterWorse++;
                    }

                    if (otherAircraft.height > aircraft.height) {
                        aircraft.heightProperty.counterBetter++;
                    } else if (otherAircraft.height === aircraft.height) {
                        aircraft.heightProperty.counterEqual++;
                    } else if (otherAircraft.height < aircraft.height) {
                        aircraft.heightProperty.counterWorse++;
                    }

                    if (otherAircraft.consumption > aircraft.consumption) {
                        aircraft.consumptionProperty.counterBetter++;
                    } else if (otherAircraft.consumption === aircraft.consumption) {
                        aircraft.consumptionProperty.counterEqual++;
                    } else if (otherAircraft.consumption < aircraft.consumption) {
                        aircraft.consumptionProperty.counterWorse++;
                    }
                }
            }
        }
        this.minData = {
            "distance": distanceMin,
            "seatCount": seatCountMin,
            "maxSeatCount": maxSeatCountMin,
            "length": lengthMin,
            "wingSpan": wingSpanMin,
            "height": heightMin,
            "consumption": consumptionMin
        };
        this.maxData = {
            "distance": distanceMax,
            "seatCount": seatCountMax,
            "maxSeatCount": maxSeatCountMax,
            "length": lengthMax,
            "wingSpan": wingSpanMax,
            "height": heightMax,
            "consumption": consumptionMax
        };
        this.distance = parseInt(distance / count);
        this.seatCount = parseInt(seatCount / count);
        this.maxSeatCount = parseInt(maxSeatCount / count);
        this.length = Math.round((length / count)*100)/100;
        this.wingSpan = Math.round((wingSpan / count)*100)/100;
        this.height = Math.round((height / count)*100)/100;
        this.consumption = Math.round((consumption / count)*100)/100;
        this.distancePercent = (100 *        (this.distance - this.minData.distance)) / (this.maxData.distance - this.minData.distance);
        this.seatCountPercent = (100 *       (this.seatCount - this.minData.seatCount)) / (this.maxData.seatCount - this.minData.seatCount);
        this.maxSeatCountPercent = (100 *    (this.maxSeatCount - this.minData.maxSeatCount)) / (this.maxData.maxSeatCount - this.minData.maxSeatCount);
        this.lengthPercent = (100 *          (this.length - this.minData.length)) / (this.maxData.length - this.minData.length);
        this.wingSpanPercent = (100 *        (this.wingSpan - this.minData.wingSpan)) / (this.maxData.wingSpan - this.minData.wingSpan);
        this.heightPercent = (100 *          (this.height - this.minData.height)) / (this.maxData.height - this.minData.height);
        this.consumptionPercent = (100 *     (this.consumption - this.minData.consumption)) / (this.maxData.consumption - this.minData.consumption);

        this.distancePercentReserve = 100 - this.distancePercent;
        this.seatCountPercentReserve = 100 - this.seatCountPercent;
        this.maxSeatCountPercentReserve = 100 - this.maxSeatCountPercent;
        this.lengthPercentReserve = 100 - this.lengthPercent;
        this.wingSpanPercentReserve = 100 - this.wingSpanPercent;
        this.heightPercentReserve = 100 - this.heightPercent;
        this.consumptionPercentReserve = 100 - this.consumptionPercent;
    }
}