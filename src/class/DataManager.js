class DataManager {

    setAsync = true;
    constructor() { // routing
        this.routing = new Routing();
        this.allAircraftObservable = new Subject();
        this.civilAircraftObservable = new Subject();
        this.militaryAircraftObservable = new Subject();
        this.allAircraftRepository = new AircraftRepository();
        this.civilAircraftRepository = new AircraftRepository();
        this.militaryAircraftRepository = new AircraftRepository();
        this.loadAllAircraftsFromLocalStorage();
        this.loadCivilAircraftsFromLocalStorage();
        this.loadMilitaryAircraftsFromLocalStorage();
        this.update();
        this.aircraftToCode();
        setInterval(() => {
            this.update();

        }, 1000);

    }

    update() {
        this.routing.updateHash();
        if (this.routing.currentView() === this.routing.map().allAircraft) {
            this.updateAllAircraftsFromAjax();
        } else if (this.routing.currentView() === this.routing.map().civilAircraft) {
            this.updateCivilAircraftsFromAjax();
        } else if (this.routing.currentView() === this.routing.map().militaryAircraft) {
            this.updateMilitaryAircraftsFromAjax();
        }

    }

    getID(ID) {
        this.routing.updateHash();

        if (this.routing.currentView() === this.routing.map().allAircraft) {
            return this.allAircraftRepository.getAircraftByID(ID);
        } else if (this.routing.currentView() === this.routing.map().civilAircraft) {
            return this.civilAircraftRepository.getAircraftByID(ID);
        } else if (this.routing.currentView() === this.routing.map().militaryAircraft) {
            return this.militaryAircraftRepository.getAircraftByID(ID);
        }

    }

    getRepository() {
        this.routing.updateHash();
        let result;
        if (this.routing.currentView() === this.routing.map().allAircraft) {
            result = this.allAircraftRepository;
        } else if (this.routing.currentView() === this.routing.map().civilAircraft) {
            result = this.civilAircraftRepository;
        } else if (this.routing.currentView() === this.routing.map().militaryAircraft) {
            result = this.militaryAircraftRepository;
        }
        return result;
    }
    getAircraftCode() {
        return this.aircraftCode;
    }
    loadAllAircraftsFromLocalStorage() {
        const data = JSON.parse(localStorage.getItem('AllData'));
        const version = localStorage.getItem('AllVersionKey');
        this.setAllAircraftRepository(data, version);
    }

    loadCivilAircraftsFromLocalStorage() {
        const version = localStorage.getItem('CivilVersionKey');
        const data = JSON.parse(localStorage.getItem('CivilData'));
        this.setCivilAircraftRepository(data, version);
    }

    loadMilitaryAircraftsFromLocalStorage() {
        const version = localStorage.getItem('MilitaryVersionKey');
        const data = JSON.parse(localStorage.getItem('MilitaryData'));
        this.setMilitaryAircraftRepository(data, version);
    }

    loadAircraftCodeFromLocalStorage() {
        return JSON.parse(localStorage.getItem('AircraftCodeData'));
    }
    updateAllAircraftsFromAjax() {
        $.ajax({
            type: 'GET',
            // url: "http://192.168.10.39/aircraft/index.php/aircrafts/version-military/" + $military_version + "/version-civil/" + $civil_version + "",
            url: "http://192.168.10.39/aircraft/index.php/aircrafts/version/" + this.allAircraftRepository.version + "",
            dataType: 'json',
            async: this.setAsync,
            success: (response) => {
                if (response !== undefined) {
                    InformationNotification.updateAircraft();
                    this.setAllAircraftRepository(response.data, response.data.version);
                    this.setLocalStorageByAllAircrafts(response.data, response.data.version);
                }
            },
            error: (response) => {

            }

        });
    }

    updateCivilAircraftsFromAjax() {
        $.ajax({
            type: 'GET',
            url: "http://192.168.10.39/aircraft/index.php/aircrafts/civil/version/" + this.civilAircraftRepository.version + "",
            dataType: 'json',
            async: this.setAsync,
            success: (response) => {
                if (response !== undefined) {
                    InformationNotification.updateAircraft();
                    this.setCivilAircraftRepository(response.data, response.data.civil.version);
                    this.setLocalStorageByCivilAircrafts(response.data, response.data.civil.version);
                }
            },
            error: (response) => {

            }

        });

    }

    updateMilitaryAircraftsFromAjax() {
        $.ajax({
            type: 'GET',
            url: "http://192.168.10.39/aircraft/index.php/aircrafts/military/version/" + this.militaryAircraftRepository.version + "",
            dataType: 'json',
            async: this.setAsync,
            success: (response) => {
                if (response !== undefined) {
                    InformationNotification.updateAircraft();
                    this.setMilitaryAircraftRepository(response.data, response.data.military.version);
                    this.setLocalStorageByMilitaryAircrafts(response.data, response.data.military.version);
                }
            },
            error: (response) => {

            }

        });

    }

    aircraftToCode() {
        const AircraftCode = this.loadAircraftCodeFromLocalStorage();
        if (AircraftCode) {
            this.aircraftCode = AircraftCode;
        } else {
            $.ajax({
                type:"GET",
                url: "http://192.168.10.39/aircraft/index.php/aircrafts/aircraftCode",
                success: (response) => {
                    this.setLocalStorageByAircraftCode(response);
                    this.aircraftCode = response;
                },
                error: function(jqXHR, textStatus, errorThrown) {

                }
            });
        }

    }

    setLocalStorageByAllAircrafts(data, version) {

        localStorage.setItem('AllVersionKey', version);
        localStorage.setItem('AllData', JSON.stringify(data));
    }

    setLocalStorageByCivilAircrafts(data, version) {
        localStorage.setItem('CivilVersionKey', version);
        localStorage.setItem('CivilData', JSON.stringify(data));
    }

    setLocalStorageByMilitaryAircrafts(data, version) {
        localStorage.setItem('MilitaryVersionKey', version);
        localStorage.setItem('MilitaryData', JSON.stringify(data));
    }

    setLocalStorageByAircraftCode(data) {
        localStorage.setItem('AircraftCodeData', JSON.stringify(data));
    }
    setAllAircraftRepository(data, version) {
        this.allAircraftRepository.version = version;
        this.allAircraftRepository.clear();
        if (data) {
            for (const rawAircraft of data.civil.aircrafts) {
                let civilAircraft = new CivilAircraft(
                    rawAircraft.name,
                    rawAircraft.distance,
                    rawAircraft.seatCount,
                    rawAircraft.maxSeatCount,
                    rawAircraft.length,
                    rawAircraft.wingSpan,
                    rawAircraft.height,
                    rawAircraft.consumption
                );
                civilAircraft.setID(rawAircraft.id);
                civilAircraft.setGateLicense(rawAircraft.gateLicense);
                this.allAircraftRepository.add(civilAircraft);
            }
            for (const rawAircraft of data.military.aircrafts) {
                let militaryAircraft = new MilitaryAircraft(
                    rawAircraft.name,
                    rawAircraft.distance,
                    rawAircraft.seatCount,
                    rawAircraft.maxSeatCount,
                    rawAircraft.length,
                    rawAircraft.wingSpan,
                    rawAircraft.height,
                    rawAircraft.consumption
                );
                militaryAircraft.setID(rawAircraft.id);
                militaryAircraft.setAirRefueling(rawAircraft.airRefueling);
                militaryAircraft.setFlare(rawAircraft.flare);


                this.allAircraftRepository.add(militaryAircraft);
            }
            this.allAircraftObservable.notify(this.allAircraftRepository.aircrafts);
            this.allAircraftRepository.start();

        }


    }

    setCivilAircraftRepository(data, version) {
        this.civilAircraftRepository.version = version;
        this.civilAircraftRepository.clear();
        if (data) {
            for (const rawAircraft of data.civil.aircrafts) {
                let civilAircraft = new CivilAircraft(
                    rawAircraft.name,
                    rawAircraft.distance,
                    rawAircraft.seatCount,
                    rawAircraft.maxSeatCount,
                    rawAircraft.length,
                    rawAircraft.wingSpan,
                    rawAircraft.height,
                    rawAircraft.consumption
                );
                civilAircraft.setID(rawAircraft.id);
                civilAircraft.setGateLicense(rawAircraft.gateLicense);
                this.civilAircraftRepository.add(civilAircraft);
            }


            this.civilAircraftObservable.notify(this.civilAircraftRepository.aircrafts);
            this.civilAircraftRepository.start();
        }


    }

    setMilitaryAircraftRepository(data, version) {
        this.militaryAircraftRepository.version = version;
        this.militaryAircraftRepository.clear();
        if (data) {
            for (const rawAircraft of data.military.aircrafts) {
                let militaryAircraft = new MilitaryAircraft(
                    rawAircraft.name,
                    rawAircraft.distance,
                    rawAircraft.seatCount,
                    rawAircraft.maxSeatCount,
                    rawAircraft.length,
                    rawAircraft.wingSpan,
                    rawAircraft.height,
                    rawAircraft.consumption
                );
                militaryAircraft.setID(rawAircraft.id);
                militaryAircraft.setAirRefueling(rawAircraft.airRefueling);
                militaryAircraft.setFlare(rawAircraft.flare);
                this.militaryAircraftRepository.add(militaryAircraft);
            }

            this.militaryAircraftObservable.notify(this.militaryAircraftRepository.aircrafts);
            this.militaryAircraftRepository.start();

        }


    }

}