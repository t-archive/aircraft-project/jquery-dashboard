class MilitaryAircraft extends Aircraft{
    setFlare(flare) {

        this.flare = flare;

    }
    setAirRefueling(airRefueling) {
        this.airRefueling = airRefueling;
    }
    getMilitaryAircraft() {
        // if (this.id !== null || this.id !== undefined) {}
        let array =  {
            "id": this.id,
            "name": this.name,
            "distance": this.distance,
            "seatCount": this.seatCount,
            "maxSeatCount": this.maxSeatCount,
            "length": this.length,
            "wingSpan": this.wingSpan,
            "height": this.height,
            "consumption": this.consumption,
            "flare": this.flare,
            "airRefueling": this.airRefueling
        };
        if (isNaN(parseInt(this.id))) {
            delete array["id"];
        }
        return array;
    }
    makeJSON() {
        let Array = {
            "aircraft": {
            }
        };
        Array.aircraft = this.getMilitaryAircraft();
        return Array;
    }
}
