class InformationNotification {


    static updateAircraft(show = false) {

        if (Action.IsEditModalOpen() === true && show === false) {
            setTimeout(() => {
                InformationNotification.updateAircraft(true);
            }, 3005);

        } else if (Action.isSwalOpen() === true) {

        } else {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                type: 'success',
                title: 'Liste wurden Aktualisiert'
            });


        }

    }
    static ajax_error($msg) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        if ($msg === false) {
            Toast.fire({
                type: 'error',
                title: 'Fehler! - Die Änderungen wurden nicht übernommen!'
            });
        } else {
            Swal.fire(
                'Fehler!',
                'Die Änderungen wurden nicht übernommen!',
                'error'
            );
        }
    }
}