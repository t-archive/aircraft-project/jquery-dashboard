class Action {
    myID = null;
    aircraft = null;

    constructor() {

    }

    static IsEditModalOpen() {
        return $('#edit-modal').is(':visible');
    }

    static IsChartsModalOpen() {
        return $('#charts-modal').is(':visible');
    }

    static FormArrayToArray(array, format = false) {
        let new_obj = {};
        $.each(array, function (i, obj) {
            let value = obj.value;
            if (format === true) {
                if (!isNaN(value)) {
                    value = Number(value);
                }
                $('#' + obj.name + ' option').each(function () {
                    const option = $(this).val();
                    if (parseInt(option) === 1 || parseInt(option) === 0) {
                        if (value === 1) {
                            value = true;
                        } else if (value === 0) {
                            value = false;
                        }
                    }
                });
            }
            new_obj[obj.name] = value;
        });
        return new_obj;
    }

    onEditAircraftClicked(id, dataManager) {
        const buildModal = new BuildModal();
        const aircraft = dataManager.getID(id);
        buildModal.resetForm();
        if (aircraft === null) {
            let routing = new Routing();
            routing.updateHash();
            routing.update();
        }
        setTimeout(() => {
            buildModal.editModal(aircraft);
            buildModal.showModal();
            this.checkIsNotTheSameInput(aircraft);
        }, 100)

    };

    chartsAircraftClicked(id, dataManager) {
        const buildModal = new BuildModal();
        buildModal.showChartsModal(true);

        this.chartsClickHTML();
        // this.chartsClickEvent(id, dataManager);
        this.chartsAircraftShowAndHandle(id, dataManager);
    }
    getGreenToRed(percent, opacity = "0.85") {
        let r = percent < 50 ? 255 : Math.floor(255 - (percent * 2 - 100) * 255 / 100);
        let g = percent > 50 ? 255 : Math.floor((percent * 2) * 255 / 100);
        return 'rgba(' + r + ',' + g + ',0,' + opacity + ')';
    }

    chartsAircraftShowAndHandle(id, dataManager) {
        let aircraftObj = dataManager.getRepository();
        let aircraft = aircraftObj.getAircraftByID(id);
        let percent = aircraftObj.compileToPercent(aircraft);
        let stat = false;


        let nameToData = {
            'Reichweite':           aircraft.distance.toLocaleString()        + " " + GeneralMemory.distanceEnd(),
            'Typische Sitzanzahl':  aircraft.seatCount.toLocaleString()       + " " + GeneralMemory.seatCountEnd(),
            'Maximale Sitzanzahl':  aircraft.maxSeatCount.toLocaleString()    + " " + GeneralMemory.maxSeatCountEnd(),
            'Länge':                aircraft.length.toLocaleString()          + " " + GeneralMemory.lengthEnd(),
            'Flügelspannweite':     aircraft.wingSpan.toLocaleString()        + " " + GeneralMemory.wingSpanEnd(),
            'Höhe':                 aircraft.height.toLocaleString()          + " " + GeneralMemory.heightEnd(),
            'Verbrauch':            aircraft.consumption.toLocaleString()     + " " + GeneralMemory.consumptionEnd()
        };

        let averageWorse = {
            'Reichweite':           aircraft.distanceProperty.counterWorse,
            'Typische Sitzanzahl':  aircraft.seatCountProperty.counterWorse,
            'Maximale Sitzanzahl':  aircraft.maxSeatCountProperty.counterWorse,
            'Länge':                aircraft.lengthProperty.counterWorse,
            'Flügelspannweite':     aircraft.wingSpanProperty.counterWorse,
            'Höhe':                 aircraft.heightProperty.counterWorse,
            'Verbrauch':            aircraft.consumptionProperty.counterBetter
        };
        let averageBetter = {
            'Reichweite':           aircraft.distanceProperty.counterBetter,
            'Typische Sitzanzahl':  aircraft.seatCountProperty.counterBetter,
            'Maximale Sitzanzahl':  aircraft.maxSeatCountProperty.counterBetter,
            'Länge':                aircraft.lengthProperty.counterBetter,
            'Flügelspannweite':     aircraft.wingSpanProperty.counterBetter,
            'Höhe':                 aircraft.heightProperty.counterBetter,
            'Verbrauch':            aircraft.consumptionProperty.counterWorse
        };
        let averageEqual = {
            'Reichweite':           aircraft.distanceProperty.counterEqual,
            'Typische Sitzanzahl':  aircraft.seatCountProperty.counterEqual,
            'Maximale Sitzanzahl':  aircraft.maxSeatCountProperty.counterEqual,
            'Länge':                aircraft.lengthProperty.counterEqual,
            'Flügelspannweite':     aircraft.wingSpanProperty.counterEqual,
            'Höhe':                 aircraft.heightProperty.counterEqual,
            'Verbrauch':            aircraft.consumptionProperty.counterEqual
        };
        let averageToData = {
            'Reichweite':           aircraftObj.averageData.distance.toLocaleString()     + " " + GeneralMemory.distanceEnd(),
            'Typische Sitzanzahl':  aircraftObj.averageData.seatCount.toLocaleString()    + " " + GeneralMemory.seatCountEnd(),
            'Maximale Sitzanzahl':  aircraftObj.averageData.maxSeatCount.toLocaleString() + " " + GeneralMemory.maxSeatCountEnd(),
            'Länge':                aircraftObj.averageData.length.toLocaleString()       + " " + GeneralMemory.lengthEnd(),
            'Flügelspannweite':     aircraftObj.averageData.wingSpan.toLocaleString()     + " " + GeneralMemory.wingSpanEnd(),
            'Höhe':                 aircraftObj.averageData.height.toLocaleString()       + " " + GeneralMemory.heightEnd(),
            'Verbrauch':            aircraftObj.averageData.consumption.toLocaleString()  + " " + GeneralMemory.consumptionEnd()
        };

        let ctx = document.getElementById('myChart').getContext('2d');

        this.myChart = new Chart(ctx, {
            type: 'polarArea',
            data: {
                labels: ['Reichweite', 'Typische Sitzanzahl', 'Maximale Sitzanzahl', 'Länge', 'Flügelspannweite', 'Höhe', 'Verbrauch'],
                datasets: [{
                    labels: "Aircraft",
                    //data: [aircraft.distance, aircraft.seatCount, aircraft.maxSeatCount, aircraft.length, aircraft.wingSpan, aircraft.height],
                    data: [
                        percent.aircraft.distance,
                        percent.aircraft.seatCount,
                        percent.aircraft.maxSeatCount,
                        percent.aircraft.length,
                        percent.aircraft.wingSpan,
                        percent.aircraft.height,
                        percent.aircraftReserve.consumption
                    ],

                    backgroundColor: [
                        this.getGreenToRed(percent.aircraft.distance),
                        this.getGreenToRed(percent.aircraft.seatCount),
                        this.getGreenToRed(percent.aircraft.maxSeatCount),
                        this.getGreenToRed(percent.aircraft.length),
                        this.getGreenToRed(percent.aircraft.wingSpan),
                        this.getGreenToRed(percent.aircraft.height),
                        this.getGreenToRed(percent.aircraftReserve.consumption)
                    ],
                    borderColor: [
                        this.getGreenToRed(percent.aircraft.distance, "1"),
                        this.getGreenToRed(percent.aircraft.seatCount, "1"),
                        this.getGreenToRed(percent.aircraft.maxSeatCount, "1"),
                        this.getGreenToRed(percent.aircraft.length, "1"),
                        this.getGreenToRed(percent.aircraft.wingSpan, "1"),
                        this.getGreenToRed(percent.aircraft.height, "1"),
                        this.getGreenToRed(percent.aircraftReserve.consumption, "1")
                    ],
                    borderWidth: 1
                }, {
                    label: 'Durchschnitt',
                    //data: [aircraft.distance, aircraft.seatCount, aircraft.maxSeatCount, aircraft.length, aircraft.wingSpan, aircraft.height],
                    data: [
                        aircraftObj.averageData.distancePercent,
                        aircraftObj.averageData.seatCountPercent,
                        aircraftObj.averageData.maxSeatCountPercent,
                        aircraftObj.averageData.lengthPercent,
                        aircraftObj.averageData.wingSpanPercent,
                        aircraftObj.averageData.heightPercent,
                        aircraftObj.averageData.consumptionPercentReserve
                    ],
                    backgroundColor: [
                        'rgba(0, 0, 0, 0.5)',
                        'rgba(0, 0, 0, 0.5)',
                        'rgba(0, 0, 0, 0.5)',
                        'rgba(0, 0, 0, 0.5)',
                        'rgba(0, 0, 0, 0.5)',
                        'rgba(0, 0, 0, 0.5)',
                        'rgba(0, 0, 0, 0.5)'
                    ],
                    borderColor: [
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0)'
                    ],
                    borderWidth: 1,

                },
                ]
            },
            options: {

                title: {
                    display: true,
                    text: aircraft.name,
                    position: 'top',
                    fontSize: 16,
                    fontColor: '#111',
                    padding: 20
                },
                scale: {

                    ticks: {
                        display: false,
                        min: 0,
                        max: 100
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function (tooltipItem, data) {
                            return data['labels'][tooltipItem[0]['index']];
                        },
                        label: (tooltipItem, data) => {
                            //return data['datasets'][0]['data'][tooltipItem['index']];

                            return "Aktuell: " + nameToData[data['labels'][tooltipItem['index']]];

                        },
                        afterLabel: (tooltipItem, data) => {

                            return "\nDurchschnitt: " + averageToData[data['labels'][tooltipItem['index']]] + "" +
                                "\nSchlechter: " + averageWorse[data['labels'][tooltipItem['index']]] + "" +
                                "\nGleichwertig: " + averageEqual[data['labels'][tooltipItem['index']]] + "" +
                                "\nBesser: " + averageBetter[data['labels'][tooltipItem['index']]] + "";


                        }
                    },
                    backgroundColor: '#FFF',
                    titleFontSize: 16,
                    titleFontColor: '#0066ff',
                    bodyFontColor: '#000',
                    bodyFontSize: 14,
                    displayColors: false
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        boxWidth: 20,
                        fontColor: '#111',
                        padding: 15
                    }
                },

                plugins: {
                    datalabels: {
                        color: '#000',
                        textAlign: 'center',
                        font: {
                            lineHeight: 1.6
                        },
                        formatter: function (value, ctx) {
                            let i = ctx.dataIndex;
                            if (ctx.dataset.backgroundColor[i] !== "rgba(0, 0, 0, 0.5)") {
                                if (value < 10) {
                                    return "";
                                } else if (value < 20) {
                                    return ctx.chart.data.labels[i].slice(0, 1);
                                } else if (value < 35) {
                                    return ctx.chart.data.labels[i].slice(0, 3) + "...";
                                } else if (value < 50) {
                                    return ctx.chart.data.labels[i].slice(0, 10) + "...";
                                } else {
                                    return ctx.chart.data.labels[i];
                                }
                            } else {
                                return "";
                            }


                        }
                    }
                }
            }
        });

        $("#reverse").on("click", (a) => {
            //console.log(a);

            if (stat === false) {
                stat = true;

                $("#reverse").html("<i class='fa fa-redo'></i>");
                $("#reverse").removeClass("btn-success");
                $("#reverse").addClass("btn-danger");
                this.myChart.data.datasets[0].data = [
                    percent.aircraftReserve.distance,
                    percent.aircraftReserve.seatCount,
                    percent.aircraftReserve.maxSeatCount,
                    percent.aircraftReserve.length,
                    percent.aircraftReserve.wingSpan,
                    percent.aircraftReserve.height,
                    percent.aircraft.consumption
                ];
                this.myChart.data.datasets[1].data = [
                    aircraftObj.averageData.distancePercentReserve,
                    aircraftObj.averageData.seatCountPercentReserve,
                    aircraftObj.averageData.maxSeatCountPercentReserve,
                    aircraftObj.averageData.lengthPercentReserve,
                    aircraftObj.averageData.wingSpanPercentReserve,
                    aircraftObj.averageData.heightPercentReserve,
                    aircraftObj.averageData.consumptionPercent
                ];

            } else {
                stat = false;
                $("#reverse").html("<i class='fa fa-redo'></i>");
                $("#reverse").removeClass("btn-danger");
                $("#reverse").addClass("btn-success");

                this.myChart.data.datasets[0].data = [
                    percent.aircraft.distance,
                    percent.aircraft.seatCount,
                    percent.aircraft.maxSeatCount,
                    percent.aircraft.length,
                    percent.aircraft.wingSpan,
                    percent.aircraft.height,
                    percent.aircraftReserve.consumption
                ];
                this.myChart.data.datasets[1].data = [
                    aircraftObj.averageData.distancePercent,
                    aircraftObj.averageData.seatCountPercent,
                    aircraftObj.averageData.maxSeatCountPercent,
                    aircraftObj.averageData.lengthPercent,
                    aircraftObj.averageData.wingSpanPercent,
                    aircraftObj.averageData.heightPercent,
                    aircraftObj.averageData.consumptionPercentReserve
                ];
            }

            this.myChart.update();

        });
        const similarAircraft = aircraftObj.getSimilarAircraft(aircraft);

        if (similarAircraft.length !== 0) {
            let htmlAircraftList = "";
            for(const item of similarAircraft) {
                htmlAircraftList += "<button " +
                    " onclick='chartRowAircraft("+ item.id + ")' " +
                    " class='btn btn-primary btn-sm btn-block mb-1' " +
                    " >"+ item.name + "" +
                    "</button>";
            }

            $("#otherChartsAircraft").html("<p><strong>Ähnliche Flugzeuge</strong></p>"+htmlAircraftList+"<br>");

        }

    }



    chartsClickHTML() {
        $("#charts-modal .modal-body").html("");
        $("#charts-modal .modal-body").html("<div class='row'><div class='col-xl-10 col-lg-12 col-md-12 col-sm-12'><canvas id=\"myChart\"></canvas></div>" +
            "<div class='d-xl-none d-md-block col-lg-12'><hr></div>" +
            "<div class='col-xl-2 col-lg-12 col-md-12 col-sm-12'><div id='otherChartsAircraft' class='col-md-12'></div></div>" +
            "<div class='col-md-12'><hr><button class='btn btn-success' id='reverse'><i class='fa fa-redo'></i></button></div>" +
            "</div>");
    }

    openCreateModal() {
        const buildModal = new BuildModal();
        buildModal.resetForm();
        buildModal.createModal();
        this.createModalButtonAnimation();
        buildModal.showModal();
    };

    createModalButtonAnimation(stat = true) {

    }

    static search(stat) {
        if (stat === true) {

            $("#search").css("width", "5%");
            $("#search").removeClass("hide");

            $("#search").animate({width: "100%"}, 500);
            $("#search-input").focus();
            this.searchStat = true;
        } else if (stat === false) {
            $("#search").animate({width: "4%"}, 250);
            setTimeout(() => {
                $("#search").addClass("hide");

            }, 225);
            this.searchStat = false;
        }
        if($("#search-input").val() !== "") {
            setInterval(() => {
                if ($("#search-input").val() !== "") {
                    $("#btnSearch").animate({'font-size': "1.50rem"}, 500);
                    $("#btnSearch").animate({'font-size': "1rem"}, 500);
                    $("#btnSearch").animate({'font-size': "1.50rem"}, 500);
                    $("#btnSearch").animate({'font-size': "1rem"}, 500);
                    $("#btnSearch").animate({'font-size': "1.50rem"}, 500);
                    $("#btnSearch").animate({'font-size': "1rem"}, 500);
                    $("#btnSearch").animate({'font-size': "1.25rem"}, 500);
                }
            }, 5000);
            $("#btnSearch").addClass("text-primary");
        } else {
            $("#btnSearch").animate({'font-size': "1.25rem"}, 250);
            $("#btnSearch").removeClass("text-primary");
        }
    }

    checkIsNotTheSameInput(aircraft) {

        BuildModal.disableSubmitButton(true);
        const form = $("#edit-aircraft");
        let form_elements = form.find(":input");
        let change = false;

        for (const myInput of form_elements) {
            if (myInput.disabled === false && myInput.name !== "" && myInput.name !== "id") {

                $('#' + myInput.id).change(() => {
                    change = true;
                    let myPostAircraft = Action.FormArrayToArray($("#edit-aircraft").serializeArray(), true);
                    this.status = false;
                    for (const input of form_elements) {
                        if (input.disabled === false && input.name !== "" && input.name !== "id") {

                            if (aircraft[input.name] !== myPostAircraft[input.name]) {
                                BuildModal.disableSubmitButton(false);
                                this.status = true;
                            } else if (aircraft[input.name] === myPostAircraft[input.name]) {
                                if (this.status !== true) {
                                    BuildModal.disableSubmitButton(true);
                                }
                            } else {

                            }

                        }

                    }
                });


            }
        }


    }

    static isSwalOpen() {
        let result;
        if ($(".swal2-shown .swal2-height-auto").length > 0) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    static controlWithKey() {
        const self = this;
        $(document).keyup(function (e) {

            if (e.key === "Escape") {
                if (Action.IsEditModalOpen() === true) {
                    if (Action.isSwalOpen() !== true) {
                        BuildModal.showModalStatic(false);
                    }
                } else if (Action.IsChartsModalOpen() === true) {
                    if (Action.isSwalOpen() !== true) {
                        BuildModal.showChartsModalStatic(false);
                    }
                }
                if (self.searchStat === true) {
                    self.search(false);
                }
            } else if (e.key === "F2" || e.key === "Insert") {
                if (Action.IsEditModalOpen() === false && Action.IsChartsModalOpen() === false) {
                    openCreateModal();
                } else if (Action.IsEditModalOpen() === true) {
                    BuildModal.showModalStatic(false);
                } else if (Action.IsChartsModalOpen() === true) {
                    BuildModal.showChartsModalStatic(false);
                }
            } else if (e.key === "Delete") {
                if (Action.IsEditModalOpen() === true) {
                    $('#edit-delete').trigger("click");
                }
            } else if (e.key === "Enter") {
                if (Action.IsEditModalOpen() === true) {
                    if ($('#edit-submit').attr("disabled") === undefined) {
                        $('#edit-submit').trigger("click");
                    }
                }
            } else if (e.key === "ArrowRight" || e.key === "F9") {
                if (self.searchStat === true && e.key === "ArrowRight") {

                } else {
                    if (Action.IsEditModalOpen() !== true && Action.IsChartsModalOpen() !== true) {
                        Action.controlNextTab();
                    }
                }

            } else if (e.key === "ArrowLeft") {
                if (self.searchStat === true && e.key === "ArrowLeft") {

                } else {
                    if (Action.IsEditModalOpen() !== true && Action.IsChartsModalOpen() !== true) {
                        Action.controlPreviousTab();
                    }
                }
            } else if (e.key === "F1") {
                if (Action.IsEditModalOpen() !== true && Action.IsChartsModalOpen() !== true) {
                    if (self.searchStat === true) {
                        self.search(false);
                    } else {
                        self.search(true);
                    }
                }

            }
        });
    }


    static controlNextTab() {
        const routing = new Routing();
        routing.updateHash();
        if (routing.currentView() === routing.map().allAircraft) {
            routing.setPage(routing.map().civilAircraft);
        } else if (routing.currentView() === routing.map().civilAircraft) {
            routing.setPage(routing.map().militaryAircraft);
        } else if (routing.currentView() === routing.map().militaryAircraft) {
            routing.setPage(routing.map().allAircraft);
        } else {
            routing.setPage(routing.map().allAircraft);
        }
        routing.update();
    }

    static controlPreviousTab() {
        const routing = new Routing();
        routing.updateHash();
        if (routing.currentView() === routing.map().allAircraft) {
            routing.setPage(routing.map().militaryAircraft);
        } else if (routing.currentView() === routing.map().civilAircraft) {
            routing.setPage(routing.map().allAircraft);
        } else if (routing.currentView() === routing.map().militaryAircraft) {
            routing.setPage(routing.map().civilAircraft);
        } else {
            routing.setPage(routing.map().allAircraft);
        }
        routing.update();
    }
    static inRange(aircraft, minmax, statAircraft) {
        let min = statAircraft - minmax;
        let max = statAircraft + minmax;
        return ((aircraft-min)*(aircraft-max) <= 0);

    }

}