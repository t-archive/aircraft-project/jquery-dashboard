class BuildModal {

    createModal() {
        $("#edit-modal .modal-title").html("<span class='text-success'>Hinzufügen</span>");
        $("#edit-modal .modal-header").addClass("bg-dark text-white");
        this.resetForm();
        this.disabledAll(true);
        this.disabledTyp(false, true);
        this.buttonAction("create");
        this.typSelectPage();

    }

    editModal(aircraft) {
        $("#edit-modal .modal-title").html("<span class='text-warning'>Bearbeiten</span>");
        $("#edit-modal .modal-header").addClass("bg-dark text-white");
        this.resetForm();
        this.buttonAction("edit");
        $("#id").val(aircraft.id);
        $("#name").val(aircraft.name);
        $("#distance").val(aircraft.distance);
        $("#seatCount").val(aircraft.seatCount);
        $("#maxSeatCount").val(aircraft.maxSeatCount);
        $("#length").val(aircraft.length);
        $("#wingSpan").val(aircraft.wingSpan);
        $("#height").val(aircraft.height);
        $("#consumption").val(aircraft.consumption);

        this.disabledAll(false);
        this.disabledTyp(true, false);


        $('#flare').prop("disabled", true);
        $('#airRefueling').prop("disabled", true);
        $('#gateLicense').prop("disabled", true);
        $('#gateLicense-modal').css("display", "inline");
        $('#flare-modal').css("display", "inline");
        $('#airRefueling-modal').css("display", "inline");


        if (aircraft instanceof MilitaryAircraft) {
            $('#flare').prop("disabled", false);
            $('#airRefueling').prop("disabled", false);
            $('#gateLicense-modal').css("display", "none");

            let flareIF = aircraft.flare;
            let airRefuelingIF = aircraft.airRefueling;
            if (flareIF === true) {
                $('#flare option[value=1]').attr('selected', 'selected');
            } else {
                $('#flare option[value=0]').attr('selected', 'selected');
            }
            if (airRefuelingIF === true) {
                $('#airRefueling option[value=1]').attr('selected', 'selected');
            } else {
                $('#airRefueling option[value=0]').attr('selected', 'selected');
            }
        } else if (aircraft instanceof CivilAircraft) {
            $('#gateLicense').prop("disabled", false);
            $('#flare-modal').css("display", "none");
            $('#airRefueling-modal').css("display", "none");

            if (aircraft.gateLicense === true) {
                $('#gateLicense option[value=1]').attr('selected', 'selected');
            } else {
                $('#gateLicense option[value=0]').attr('selected', 'selected');
            }

        }

    }

    disabledAll(stat = true) {
        $("#name").prop("disabled", stat);
        $("#distance").prop("disabled", stat);
        $("#seatCount").prop("disabled", stat);
        $("#maxSeatCount").prop("disabled", stat);
        $("#length").prop("disabled", stat);
        $("#wingSpan").prop("disabled", stat);
        $("#height").prop("disabled", stat);
        $("#consumption").prop("disabled", stat);
    }
    disabledAllExtra(stat = true) {

        $("#airRefueling").prop("disabled", stat);
        $("#flare").prop("disabled", stat);
        $("#gateLicense").prop("disabled", stat);
    }
    resetForm() {

        $('#edit-aircraft').trigger("reset");
        $('#flare').prop("disabled", false);
        $('#airRefueling').prop("disabled", false);
        $('#gateLicense').prop("disabled", false);
        BuildModal.disableSubmitButton(false);
        $('#flare option[value=1]').attr('selected', false);
        $('#flare option[value=0]').attr('selected', false);
        $('#airRefueling option[value=1]').attr('selected', false);
        $('#airRefueling option[value=0]').attr('selected', false);
        $('#gateLicense option[value=1]').attr('selected', false);
        $('#gateLicense option[value=0]').attr('selected', false);

        $('#flare').prop("disabled", true);
        $('#airRefueling').prop("disabled", true);
        $('#gateLicense').prop("disabled", true);
        $('#gateLicense-modal').css("display", "inline");
        $('#flare-modal').css("display", "inline");
        $('#airRefueling-modal').css("display", "inline");

        $('#gateLicense-modal').css("display", "none");
        $('#flare-modal').css("display", "none");
        $('#airRefueling-modal').css("display", "none");


    }

    disabledTyp(stat = true, show = true) {
        $("#typ").prop("disabled", stat);
        if (show === false) {
            $('#typ-div').css("display", "none");
        } else {
            $('#typ-div').css("display", "inline");
        }
    }

    showModal(stat = true) {
        if (stat === false) {
            $('#edit-modal').modal('toggle');
        } else {
            $('#edit-modal').modal('show');
        }
    }
    showChartsModal(stat = true) {
        $("#charts-modal .modal-title").html("<span class='text-light'>Übersicht</span>");
        $("#charts-modal .modal-header").addClass("bg-dark text-white");
        if (stat === false) {
            $('#charts-modal').modal('toggle');
        } else {
            $('#charts-modal').modal('show');
        }
    }
    static showModalStatic(stat) {
        const myBuildModalVar = new BuildModal();
        myBuildModalVar.showModal(stat);
    }
    static showChartsModalStatic(stat) {
        const myBuildModalVar = new BuildModal();
        myBuildModalVar.showChartsModal(stat);
    }
    getTextInput(value, disabled = true) {
        let action = false;
        if (value === "military") {
            $('#flare').prop("disabled", false);
            $('#flare-modal').css("display", "inline");
            $('#airRefueling').prop("disabled", false);
            $('#airRefueling-modal').css("display", "inline");
            $('#gateLicense-modal').css("display", "none");
            action = true;
        } else if (value === "civil") {
            $('#gateLicense').prop("disabled", false);
            $('#gateLicense-modal').css("display", "inline");

            $('#flare-modal').css("display", "none");
            $('#airRefueling-modal').css("display", "none");
            action = true;
        }

        if (action === true && disabled === true) {
            this.disabledAll(false);
        }
    }


    buttonAction(site) {
        const delete_btn = $("#edit-delete");
        const delete_div = $("#delete-btn-div");
        const submit_btn = $("#edit-submit");
        const submit_div = $("#submit-btn-div");

        if (site === "create") {
            delete_btn.css("display", "none");
            delete_div.removeClass("col-md-3");
            submit_div.removeClass("col-md-9");
            submit_div.addClass("col-md-12");
            submit_btn.html("Erstellen");
            submit_btn.removeClass("btn-primary");
            submit_btn.addClass("btn-success");
        } else if (site === "edit") {
            delete_btn.css("display", "inline");
            delete_div.addClass("col-md-3");
            submit_div.addClass("col-md-9");
            submit_div.removeClass("col-md-12");
            submit_btn.html("Senden");
            submit_btn.addClass("btn-primary");
            submit_btn.removeClass("btn-success");
        } else {
            delete_btn.css("display", "inline");
            delete_div.addClass("col-md-3");
            submit_div.addClass("col-md-9");
            submit_div.removeClass("col-md-12");
            submit_btn.html("Senden");
            submit_btn.addClass("btn-primary");
            submit_btn.removeClass("btn-success");
        }
    }
    static disableSubmitButton(stat = true) {
        $("#edit-submit").prop("disabled", stat);
    }
    typSelectPage() {
        const routing = new Routing();
        routing.updateHash();
        if(routing.currentView() === routing.map().civilAircraft) {
            this.resetForm();
            $('#typ option[value=civil]').attr('selected','selected');
            $('#typ option[value=military]').attr('selected',false);
            selectCreateAircraft("civil");
        } else if (routing.currentView() === routing.map().militaryAircraft) {
            this.resetForm();
            $('#typ option[value=civil]').attr('selected',false);
            $('#typ option[value=military]').attr('selected','selected');
            selectCreateAircraft("military");
        } else {
            this.resetForm();
            $("#typ").val($("#typ option:first").val());
            $("#typ").trigger("reset");;


        }
    }

}