class DataCreator {
    constructor(buildModal) {
        $("#edit-aircraft #edit-submit").click(() => {
            this.formToBackend();
        });
        $("#edit-aircraft #edit-delete").click(() => {
            this.delete();
        });
        this.buildModal = buildModal;
    }
    delete() {
        let data = $("#edit-aircraft").serializeArray();
        let myPostAircraft = Action.FormArrayToArray(data);

        if (!isNaN(parseInt(myPostAircraft.id))) {
            this.formToDelete(parseInt(myPostAircraft.id), myPostAircraft);
        } else {

        }

    }
    formToDelete(id, postAircraft) {
        let name = postAircraft.name;
        if(name === undefined) {
            return;
        }
        Swal.fire({
            title: 'Löschen ' + name + '?',
            text: 'Wollen Sie das Flugzeug ' + name + ' Löschen?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ja, ich will Löschen',
            cancelButtonText: 'Abbrechen',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    url: 'http://192.168.10.39/aircraft/index.php/aircrafts/id/' + id + '',
                    type: 'DELETE',
                    dataType: "json",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    success: (result) => {
                        this.buildModal.showModal(false);

                        if (result.data.delete === false) {
                            Swal.fire(
                                'Fehler',
                                'Das Flugzeug "' + name + '" wurde nicht gelöscht! - <code>'+ JSON.stringify(result.data)  +'</code>',
                                'error'
                            )
                        } else {
                            Swal.fire(
                                'Gelöscht!',
                                'Das Flugzeug "' + name + '" wurde gelöscht!',
                                'success'
                            );
                        }
                    },
                    error: (error) => {
                        this.buildModal.showModal(false);
                        Swal.fire(
                            'Fehler',
                            'Das Flugzeug "' + name + '" wurde nicht gelöscht!<br>Da ein Fehler vorliegt! - <code>'+ JSON.stringify(error)  +'</code>',
                            'error'
                        )
                    }
                });
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        })

    }
    formToBackend() {
        let data = $("#edit-aircraft").serializeArray();
        let myPostAircraft = Action.FormArrayToArray(data);
        if (myPostAircraft.typ === "military" || myPostAircraft.typ === "civil") {
            this.formToCreateData(data);
        } else {
            this.formToEditData(data);
        }
    }
    formToCreateData(inputData) {
        let postAircraft = Action.FormArrayToArray(inputData);



        let name = postAircraft.name;
        let distance = parseInt(postAircraft.distance);
        let seatCount = parseInt(postAircraft.seatCount);
        let maxSeatCount = parseInt(postAircraft.maxSeatCount);
        let length = parseFloat(postAircraft.length);
        let wingSpan = parseFloat(postAircraft.wingSpan);
        let height = parseFloat(postAircraft.height);
        let consumption = parseFloat(postAircraft.consumption);
        let array = {};
        let tmp;
        tmp = postAircraft.typ;

        if (tmp === "civil") {
            let gateLicense = (parseInt(postAircraft.gateLicense) === 1);
            const civilAircraft = new CivilAircraft(
                name,
                distance,
                seatCount,
                maxSeatCount,
                length,
                wingSpan,
                consumption,
                height
            );
            civilAircraft.setGateLicense(gateLicense);
            array = civilAircraft.makeJSON();
        } else if (tmp === "military") {
            let flare = (parseInt(postAircraft.flare) === 1);
            let airRefueling = (parseInt(postAircraft.airRefueling) === 1);
            const militaryAircraft = new MilitaryAircraft(
                name,
                distance,
                seatCount,
                maxSeatCount,
                length,
                wingSpan,
                consumption,
                height
            );
            militaryAircraft.setFlare(flare);
            militaryAircraft.setAirRefueling(airRefueling);
            array = militaryAircraft.makeJSON();
        }


        let JSON_AJAX = $.ajax({
            method: "POST",
            url: 'http://192.168.10.39/aircraft/index.php/aircrafts/' + tmp,
            contentType: "application/json",
            data: JSON.stringify(array),
            dataType: "json",
            cache: false,
            timeout: 4000, // 4 seconds
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });


        JSON_AJAX.done((data) => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                type: 'success',
                title: 'Erfolgreich Gespeichert!'
            });

            this.buildModal.showModal(false);

        }).fail(function (jqXHR, textStatus, errorThrown) {
            InformationNotification.ajax_error();

        });


    };

    formToEditData(inputData) {
        this.buildModal = new BuildModal();
        let postAircraft = Action.FormArrayToArray(inputData);
        let id = parseInt(postAircraft.id);
        let name = postAircraft.name;
        let distance = parseInt(postAircraft.distance);
        let seatCount = parseInt(postAircraft.seatCount);
        let maxSeatCount = parseInt(postAircraft.maxSeatCount);
        let length = parseFloat(postAircraft.length);
        let wingSpan = parseFloat(postAircraft.wingSpan);
        let height = parseFloat(postAircraft.height);
        let consumption = parseFloat(postAircraft.consumption);
        let array = {};
        let tmp;

        if ((postAircraft.gateLicense) !== undefined) {
            tmp = "civil";
            let gateLicense = postAircraft.gateLicense;
            const civilAircraft = new CivilAircraft(
                name,
                distance,
                seatCount,
                maxSeatCount,
                length,
                wingSpan,
                height,
                consumption
            );
            civilAircraft.setGateLicense(gateLicense);
            civilAircraft.setID(id);
            array = civilAircraft.makeJSON();

        } else if ((postAircraft.airRefueling) !== undefined && (postAircraft.flare) !== undefined) {
            tmp = "military";
            let flare = postAircraft.flare;
            let airRefueling = postAircraft.airRefueling;
            const militaryAircraft = new MilitaryAircraft(
                name,
                distance,
                seatCount,
                maxSeatCount,
                length,
                wingSpan,
                height,
                consumption
            );
            militaryAircraft.setFlare(flare);
            militaryAircraft.setAirRefueling(airRefueling);
            militaryAircraft.setID(id);
            array = militaryAircraft.makeJSON();

        }

        let JSON_AJAX = $.ajax({
            method: "PUT",
            url: 'http://192.168.10.39/aircraft/index.php/aircrafts/' + tmp,
            contentType: "application/json",
            data: JSON.stringify(array),
            dataType: "json",
            cache: false,
            timeout: 4000, // 4 seconds
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });


        JSON_AJAX.done(() => {

            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                type: 'success',
                title: 'Erfolgreich Gespeichert!'
            });
            this.buildModal.disabledAll(true);
            this.buildModal.disabledAllExtra(true);
            setTimeout(() => {
                this.buildModal.showModal(false);
            }, 250);


        }).fail(function (jqXHR, textStatus, errorThrown) {
            InformationNotification.ajax_error();

        });


    };


}